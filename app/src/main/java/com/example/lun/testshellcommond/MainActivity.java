package com.example.lun.testshellcommond;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.TextView;

import com.example.lun.testshellcommond.target.HaiCaoApp;
import com.example.lun.testshellcommond.target.HuaShengTopApp;
import com.example.lun.testshellcommond.target.LuJinSuoApp;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        TextView tv_test = findViewById(R.id.tv_test);
        tv_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HaiCaoApp app = new HaiCaoApp();
                app.RunScript();
            }
        });

        TextView tv_test2 = findViewById(R.id.tv_test2);
        tv_test2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HuaShengTopApp app = new HuaShengTopApp();
                app.RunScript();
            }
        });
        TextView tv_test3 = findViewById(R.id.tv_test3);
        tv_test3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LuJinSuoApp app = new LuJinSuoApp();
                app.RunScript();
            }
        });
    }


//    public boolean isEmulator() {
//        String url = "tel:" + "123456";
//        Intent intent = new Intent();
//        intent.setData(Uri.parse(url));
//        intent.setAction(Intent.ACTION_DIAL);
//        // 是否可以处理跳转到拨号的 Intent
//        boolean canResolveIntent = intent.resolveActivity(this.getPackageManager()) != null;
//
//        return Build.FINGERPRINT.startsWith("generic")
//                || Build.FINGERPRINT.toLowerCase().contains("vbox")
//                || Build.FINGERPRINT.toLowerCase().contains("test-keys")
//                || Build.MODEL.contains("google_sdk")
//                || Build.MODEL.contains("Emulator")
//                || Build.SERIAL.equalsIgnoreCase("unknown")
//                || Build.SERIAL.equalsIgnoreCase("android")
//                || Build.MODEL.contains("Android SDK built for x86")
//                || Build.MANUFACTURER.contains("Genymotion")
//                || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
//                || "google_sdk".equals(Build.PRODUCT)
//                || ((TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE))
//                .getNetworkOperatorName().toLowerCase().equals("android")
//                || !canResolveIntent;
//    }


}
