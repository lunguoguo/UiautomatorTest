package com.example.lun.testshellcommond;

import android.app.Application;

//import com.mob.MobSDK;

/**
 * 作者：wl on 2017/9/14 17:35
 * 邮箱：wanglun@stosz.com
 */
public class AppLoader extends Application {
    private static Application mInstance;


    public static Application getInstance() {
        return mInstance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }


}
