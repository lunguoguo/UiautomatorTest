package com.example.lun.testshellcommond;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.util.ArrayMap;
import android.util.Log;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Random;

import static android.content.Context.ACTIVITY_SERVICE;

public class BaseCommond {
    public static String COMMOND_INTO_MODE = "adb shell";
    public static String COMMOND_BACK = "input keyevent KEYCODE_BACK";
    public static String COMMOND_START_HAICAO = "am start -n com.billionstech.grassbook/.business.main.MainActivity";
    public static String COMMOND_START_HUASHENG = "am start -n com.xcm.huasheng/.ui.activity.SplashActivity";
    public static String COMMOND_START_LUNJINSUO = "am start -n com.lufax.android/.activity.HomeActivity";
    public static void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static int getRadomDown(int base, int maxSize) {
        int bigPosition = base - new Random().nextInt(maxSize);
        return bigPosition;
    }

    public static int getRadomUp(int base, int maxSize) {
        int smollPosition = base + new Random().nextInt(maxSize);
        return smollPosition;
    }

    public static String Commond_tap(int x, int y) {
        return "input tap " + getRadomDown(x, 50) + " " + getRadomUp(y, 50);
    }

    //上啦滑动
    public static String Commond_pullUp(int x, int y, int minSize) {
        int reallX = getRadomDown(x, 1);
        Log.d("测试滑动：", "数据：");
        Log.d("测试滑动：", "x：" + x);
        Log.d("测试滑动：", "y：" + y);
        Log.d("测试滑动：", "minSize：" + minSize);
        Log.d("测试滑动：", "实际滑动：" + (getRadomUp(y, minSize) - getRadomDown(y - minSize, minSize)));
        return "input touchscreen swipe " + reallX + " " + getRadomUp(y, minSize) + " " + reallX + " " + getRadomDown(y - minSize, minSize) + " ";
    }
    //下啦滑动
    public static String Commond_pullDown(int x, int y, int minSize) {
        int reallX = getRadomDown(x, 1);
        Log.d("测试滑动：", "数据：");
        Log.d("测试滑动：", "x：" + x);
        Log.d("测试滑动：", "y：" + y);
        Log.d("测试滑动：", "minSize：" + minSize);
        Log.d("测试滑动：", "实际滑动：" + (getRadomUp(y, minSize) - getRadomDown(y - minSize, minSize)));
        return "input touchscreen swipe " + reallX + " " + getRadomUp(y, minSize) + " " + reallX + " " + getRadomDown(y - minSize, minSize) + " ";
    }

    /*
     *
     * 得到栈顶的activity
     * 路径+类名
     * 需要在清单文件中添加权限 <uses-permission android:name = “android.permission.GET_TASKS”/>
     * */

    public static String getTopActivity() {

        ActivityManager manager = (ActivityManager) AppLoader.getInstance().getSystemService(ACTIVITY_SERVICE);

        List<ActivityManager.RunningTaskInfo> runningTaskInfos = manager.getRunningTasks(1);
        ComponentName componentName;
        if (runningTaskInfos != null) {
            componentName = runningTaskInfos.get(0).topActivity;
            String result = componentName.getPackageName() + "." + componentName.getClassName();
            Log.d("栈顶抓取", "当前的栈顶是：" + result);
            return result;
        } else {
            return null;
        }
    }


    private void test() {
        ActivityManager manager = (ActivityManager) AppLoader.getInstance().getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfos = manager.getRunningTasks(1);
        ComponentName componentName = runningTaskInfos.get(0).topActivity;

        try {
            Class clz = AppLoader.getInstance().getClass().forName("android.app.ActivityThread");
            Method meth = clz.getMethod("currentActivityThread");
            Object currentActivityThread = meth.invoke(null);
            Field f = clz.getDeclaredField("mActivities");
            f.setAccessible(true);
            ArrayMap obj = (ArrayMap) f.get(currentActivityThread);
            for (Object key : obj.keySet()) {
                Object activityRecord = obj.get(key);
                Field actField = activityRecord.getClass().getDeclaredField("activity");
                actField.setAccessible(true);
                Object activity = actField.get(activityRecord);

                String act1N = activity.getClass().toString();// class
// com.example.calledjar.MainActivity
                String act1Name = act1N.substring(6);// class
// com.example.calledjar.MainActivity


            }


        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {

        }

    }


}
