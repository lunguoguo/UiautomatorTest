package com.example.lun.testshellcommond.target;

import com.example.lun.testshellcommond.BaseCommond;
import com.example.lun.testshellcommond.ShellCommandExecutor;

import static com.example.lun.testshellcommond.BaseCommond.sleep;

public class LuJinSuoApp extends BaseTarget {
    @Override
    public void RunScript() {
        ShellCommandExecutor executor = new ShellCommandExecutor();
        executor.execute("am instrument -w -r  -e debug false -e class 'com.example.lun.testshellcommond.LuJinSuoAppScript' com.example.lun.testshellcommond.test/android.support.test.runner.AndroidJUnitRunner");
    }
}
