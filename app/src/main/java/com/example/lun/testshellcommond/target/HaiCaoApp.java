package com.example.lun.testshellcommond.target;

import com.example.lun.testshellcommond.BaseCommond;
import com.example.lun.testshellcommond.MainActivity;
import com.example.lun.testshellcommond.ShellCommandExecutor;

import static com.example.lun.testshellcommond.BaseCommond.sleep;

public class HaiCaoApp extends BaseTarget {

    /*
    *   梳理：
    *     1.读取文章50次，循环
    *     2.如果不在详情页就滑动再点击，否侧 轻微的滑动详情页，保持阅读状态25秒以上
    *     3.单次阅读完退出，然后下次循环
    *
    *
    *
    * */

    //文章阅读次数
    private final int readAritcleTimes = 50;
    //详情页保持滚动次数
    private final int keepScollTimes = 25;

    @Override
    public void RunScript() {
        ShellCommandExecutor executor = new ShellCommandExecutor();
        executor.execute(BaseCommond.COMMOND_INTO_MODE);
        executor.execute(BaseCommond.COMMOND_START_HAICAO);
        sleep(startWaitTime);
        for (int i = 0; i < readAritcleTimes; ) {
            if (BaseCommond.getTopActivity().contains("FindDetailActivity")) {
                for (int j = 0; j < keepScollTimes; j++) {
                    sleep();
                    executor.execute(BaseCommond.Commond_pullUp(2, getHight() / 2, 10));
                }
                i++;
                executor.execute(BaseCommond.COMMOND_BACK);
            } else {
                sleep();
                executor.execute(BaseCommond.Commond_pullUp(50, getHight() / 2, getHight() / 4));
                executor.execute(BaseCommond.Commond_tap(getWidth() / 2, getHight() / 2));
            }

        }
    }
}
