package com.example.lun.testshellcommond.target;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.example.lun.testshellcommond.AppLoader;

public abstract class BaseTarget {
    private int width;
    private int hight;
    public final long startWaitTime = 5000;


    public BaseTarget() {
        WindowManager wm = (WindowManager) AppLoader.getInstance().getSystemService(Context.WINDOW_SERVICE);
        this.width = wm.getDefaultDisplay().getWidth();
        this.hight = wm.getDefaultDisplay().getHeight();

    }


    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHight() {
        return hight;
    }

    public void setHight(int hight) {
        this.hight = hight;
    }

    public abstract void RunScript();
}
