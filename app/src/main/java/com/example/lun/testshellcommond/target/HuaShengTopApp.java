package com.example.lun.testshellcommond.target;

import android.app.Activity;

import com.example.lun.testshellcommond.BaseCommond;
import com.example.lun.testshellcommond.ShellCommandExecutor;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import static com.example.lun.testshellcommond.BaseCommond.sleep;

public class HuaShengTopApp extends BaseTarget {

    @Override
    public void RunScript() {
        ShellCommandExecutor executor = new ShellCommandExecutor();
        executor.execute("am instrument -w -r  -e debug false -e class 'com.example.lun.testshellcommond.HuaShengAppScript' com.example.lun.testshellcommond.test/android.support.test.runner.AndroidJUnitRunner");
    }


}
