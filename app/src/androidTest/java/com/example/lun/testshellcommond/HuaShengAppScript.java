package com.example.lun.testshellcommond;


import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.UiWatcher;

import org.junit.Test;
import org.junit.runner.RunWith;

import static com.example.lun.testshellcommond.BaseCommond.sleep;

@RunWith(AndroidJUnit4.class)
public class HuaShengAppScript {
    //文章阅读次数
    private final int readAritcleTimes = 50;
    //详情页保持滚动次数
    private final int keepScollTimes = 20 * 4;

    @Test
    public void runScript() throws UiObjectNotFoundException {
        UiDevice muiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
//      muiDevice.registerWatcher  //注册监听api，不太会用


        ShellCommandExecutor executor = new ShellCommandExecutor();
        executor.execute(BaseCommond.COMMOND_INTO_MODE);
        executor.execute(BaseCommond.COMMOND_START_HUASHENG);
        sleep(3000);
        //启动等三秒，让app处理加载
        clickViewIfExistsById("com.xcm.huasheng:id/ll_close");
        clickViewIfExistsById("com.xcm.huasheng:id/tv_close");
        //正常情况还是启动页，点击 关闭广告按钮，点击 关闭广告详情页按钮



        while (!BaseCommond.getTopActivity().contains("MainActivity")) {
            sleep(2000);
            //如果不是主页就是还未处理的其他情况，这里暂时先秒面，后面可以做 发消息，让管理员来看看
        }

        clickViewIfExistsById("com.xcm.huasheng:id/image_close");
        //关闭绑定微信的悬浮框

        if (clickViewIfExistsById("检测到更新弹框的情况先不处理")) {
            //通知管理员
        }

        for (int i = 0; i < readAritcleTimes; ) {
            //开始读取文章循环
            sleep(3000);
            clickViewIfExistsById("com.xcm.huasheng:id/bt_refresh_view");
            clickViewIfExistsById("com.tencent.mm:id/jc");
            clickViewIfExistsById("com.xcm.huasheng:id/rl_baozangss");
            clickViewIfExistsById("com.xcm.huasheng:id/iv_public_close");
            //  1.重新刷新按钮 2.微信登录后退按钮  3.藏在文章中的送积分 按钮  4.获取到积分关闭按钮
            //   以上四个，如果有就要按
            if (BaseCommond.getTopActivity().contains("NewsDetailActivity")) {
                boolean isQuickByError = false;
                for (int j = 0; j < keepScollTimes; j++) {
                    sleep();
                    if (checkIfTextShowed("网络异常")) {
                        isQuickByError = true;
                        //详情里出现网络异常直接退出
                        break;
                    } else if (clickViewIfExistsById("com.xcm.huasheng:id/tv_reading")) {
                        //详情里弹框告诉你不再给分了 直接退出
                        break;
                    } else {
                        //滑动详情页
                        executor.execute(BaseCommond.Commond_pullUp(2, muiDevice.getDisplayHeight() / 2, 10));
                    }
                }
                if (!isQuickByError) i++;   //如果不是因为错误退出就表示又读了一篇文章
                executor.execute(BaseCommond.COMMOND_BACK); //无论怎么样，这次阅读结束了
            } else if (BaseCommond.getTopActivity().contains("DGTYDetailActivity") || BaseCommond.getTopActivity().contains("AdsDetailActivity")||BaseCommond.getTopActivity().contains("RedPagerActivity")) {
                executor.execute(BaseCommond.COMMOND_BACK); //进入了非新闻详情要后退
            } else {
                executor.execute(BaseCommond.Commond_pullUp(50, muiDevice.getDisplayHeight() / 2, muiDevice.getDisplayHeight() / 4));
                executor.execute(BaseCommond.Commond_tap(muiDevice.getDisplayWidth() / 2, muiDevice.getDisplayHeight() / 2));
            }

        }

    }

    private boolean clickViewIfExistsById(String id) throws UiObjectNotFoundException {
        UiObject object = new UiObject(new UiSelector().resourceId(id));
        boolean isexists = object.exists();
        if (isexists) {
            object.clickAndWaitForNewWindow();
        }
        return isexists;
    }

    private boolean checkIfTextShowed(String test) throws UiObjectNotFoundException {
        UiObject object = new UiObject(new UiSelector().textContains("网络异常"));
        return object.exists();
    }
}
