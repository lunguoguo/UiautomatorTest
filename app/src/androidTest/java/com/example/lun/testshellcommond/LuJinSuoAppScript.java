package com.example.lun.testshellcommond;


import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.util.Log;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;

import static com.example.lun.testshellcommond.BaseCommond.sleep;

@RunWith(AndroidJUnit4.class)
public class LuJinSuoAppScript {
    boolean keepRunning = true;
    String lastTimePrice = "";
    int minPrice = 1;
    int maxPrice = 4000;

    /*打开陆金所，登陆然后停留在首页，再跑这个脚本*/

    @Test
    public void runScript() throws UiObjectNotFoundException {

        UiDevice muiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
//      muiDevice.registerWatcher  //注册监听api，不太会用

        ShellCommandExecutor executor = new ShellCommandExecutor();
        executor.execute(BaseCommond.COMMOND_INTO_MODE);
        executor.execute(BaseCommond.COMMOND_START_LUNJINSUO);
        sleep(1000);

        clickViewByContents("会员交易");
        sleep(3000);


        while (keepRunning) {
            if (!BaseCommond.getTopActivity().contains("FinanceListActivity")) {
                executor.execute(BaseCommond.COMMOND_START_LUNJINSUO);
                sleep(1000);
                clickViewByContents("会员交易");
                sleep(3000);
            }
            sleep(500);
            muiDevice.swipe(muiDevice.getDisplayWidth() / 2, muiDevice.getDisplayHeight() / 3, muiDevice.getDisplayWidth() / 2, muiDevice.getDisplayHeight() * 254 / 255, 10);
            UiObject object = new UiObject(new UiSelector().resourceId("com.lufax.android:id/list_view"));
            if (object != null && object.exists()) {
                int childCount = object.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    UiObject priceObject = object.getChild(new UiSelector().instance(i).resourceId("com.lufax.android:id/tv_rate"));
                    if (priceObject.exists()) {
                        String priceText = priceObject.getText().replace(",", "");
                        Log.d("TAG", "当前金额：" + priceText);
                        if (!lastTimePrice.equals(priceText) && Double.parseDouble(priceText) > minPrice && Double.parseDouble(priceText) < maxPrice) {
                            lastTimePrice = priceText;
                            priceObject.clickAndWaitForNewWindow();
                            //boolean isInRootControllerActivity=BaseCommond.getTopActivity().contains("RootControllerActivity");
                            boolean isOK = false;
                            boolean isBack = false;
                            boolean isBack2 = false;
                            while (!isOK && !isBack && !isBack2) {
                                isOK = checkIfDesContentShowed("立即投资");
                                isBack = checkIfDesContentShowed("查看同类可投项目");
                                isBack2 = checkIfIdExists("com.lufax.android:id/btn_ok");
                                if (isBack2) {
                                    clickViewById("com.lufax.android:id/btn_ok");
                                    sleep(1000);
                                    clickViewById("com.lufax.android:id/btn_ok");
                                } else if (isBack) {
                                    muiDevice.pressBack();
                                } else if (isOK) {
                                    clickViewByContents("立即投资");
                                    boolean shouldWait = true;
                                    while (shouldWait) {
                                        if (BaseCommond.getTopActivity().contains("InvestBaseActivity")) {
                                            shouldWait = false;
                                            Log.d("tag", "进入确认页1"+System.currentTimeMillis());
                                            sleep(500);
                                            Log.d("tag", "进入确认页2"+System.currentTimeMillis());
                                            clickViewById("com.lufax.android:id/chkEnable");//勾选同意协议
                                            Log.d("tag", "进入确认页3"+System.currentTimeMillis());
                                            clickViewById("com.lufax.android:id/btn_invest");//点击投资按钮
                                            Log.d("tag", "进入确认页4"+System.currentTimeMillis());
                                            clickViewById("com.lufax.android:id/btn_next");
                                            Log.d("tag", "进入确认页5"+System.currentTimeMillis());
                                            setTextToViewById("yourPasswor", "com.lufax.android:id/s_edit");
                                            Log.d("tag", "进入确认页6"+System.currentTimeMillis());
                                            clickViewById("com.lufax.android:id/btn_confirm");
                                            return;
                                        } else if (BaseCommond.getTopActivity().contains("FinanceListActivity")) {
                                            shouldWait = false;
                                        } else if (checkIfIdExists("com.lufax.android:id/btn_left")) {
                                            clickViewById("com.lufax.android:id/btn_left");
                                            shouldWait = false;
                                        } else {
                                            isBack2 = checkIfIdExists("com.lufax.android:id/btn_ok");
                                            if (isBack2) {
                                                clickViewById("com.lufax.android:id/btn_ok");
                                                sleep(1000);
                                                isBack2 = checkIfIdExists("com.lufax.android:id/btn_ok");
                                                if (isBack2) {
                                                    clickViewById("com.lufax.android:id/btn_ok");
                                                } else {
                                                    muiDevice.pressBack();
                                                }
                                                shouldWait = false;
                                            }
                                        }
                                    }
                                } else if (BaseCommond.getTopActivity().contains("FinanceListActivity")) {
                                    isBack = true;
                                }
                            }

                        }
                    }
                }
            }
        }


    }


    private boolean clickViewById(String id) throws UiObjectNotFoundException {
        UiObject object = new UiObject(new UiSelector().resourceId(id));
        boolean isexists = object.exists();
        if (isexists) {
            object.clickAndWaitForNewWindow();
        }
        return isexists;
    }
//
//    private boolean clickViewIfExistsByContentFromPid(String pid, String text) throws UiObjectNotFoundException {
//        UiObject object = new UiObject(new UiSelector().resourceId("com.lufax.android:id/home_root"));
//        UiObject objectChlid = object.getChild(new UiSelector().instance(0));
//        UiObject item = objectChlid.getChild(new UiSelector().descriptionContains(text));
//        boolean isexists = item.exists();
//        if (isexists) {
//            item.clickAndWaitForNewWindow();
//            //Log.d("TAG","item:"+item.getSelector().dget);
//        }
//        return isexists;
//    }

    private boolean clickViewByContents(String text) throws UiObjectNotFoundException {
        UiObject object = new UiObject(new UiSelector().descriptionContains(text));
        boolean isexists = object.exists();
        if (isexists) {
            object.clickAndWaitForNewWindow();
            //Log.d("TAG","item:"+item.getSelector().dget);
        }
        return isexists;
    }

    private boolean checkIfDesContentShowed(String test) throws UiObjectNotFoundException {
        UiObject object = new UiObject(new UiSelector().descriptionContains(test));
        return object.exists();
    }

    private boolean checkIfTextShowed(String test) throws UiObjectNotFoundException {
        UiObject object = new UiObject(new UiSelector().text(test));
        return object.exists();
    }


    private boolean checkIfIdExists(String test) throws UiObjectNotFoundException {
        UiObject object = new UiObject(new UiSelector().resourceId(test));
        return object.exists();
    }

    private void setTextToViewById(String text, String id) throws UiObjectNotFoundException {
        UiObject object = new UiObject(new UiSelector().resourceId(id));
        if (object.exists()) {
            object.setText(text);
        }
    }
}
